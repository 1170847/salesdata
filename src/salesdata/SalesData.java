package salesdata;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class SalesData {

    static float weeklyIncome[] = new float[7];
    static float productTurnover[] = new float[41];

    public static void main(String[] args) {
        loadData(getData());
        showData();
        //testgetYear();
    }

    public static void loadData(JSONArray jsonArray) {
        for (Object obj : jsonArray) {
            // Extract each JSONObject 
            JSONObject tmpObj = (JSONObject) obj;
            getProductIDInFloat(tmpObj);
            getOrderDate(tmpObj);
            getDate(getOrderDate(tmpObj));
            getPriceInFloat(tmpObj);
            calcWeeklyIncome(getDate(getOrderDate(tmpObj)), getPriceInFloat(tmpObj));
            calcProductTurnover(getProductIDInFloat(tmpObj), getPriceInFloat(tmpObj));
        }
    }

    public static void showData() {
        System.out.println("----------------WEEKLY INCOMES: ----------------------");
        System.out.println("Monday " + weeklyIncome[0] + "€");
        System.out.println("Tuesday " + weeklyIncome[1] + "€");
        System.out.println("Wednesday " + weeklyIncome[2] + "€");
        System.out.println("Thursday " + weeklyIncome[3] + "€");
        System.out.println("Friday " + weeklyIncome[4] + "€");
        System.out.println("Saturday " + weeklyIncome[5] + "€");
        System.out.println("Sunday " + weeklyIncome[6] + "€");
        System.out.println("----------------PRODUCT TURNOVERS: --------------------");
        for (int i = 0; i < productTurnover.length; i++) {
            if (productTurnover[i] != 0) {
                System.out.println("The total Turnover of the product " + i + " is " + productTurnover[i] + "€");
            }
        }
    }

    public static JSONArray getData() {
        try {
            JSONParser parser = new JSONParser();
            JSONArray jsonArray = (JSONArray) parser.parse(new FileReader("salesdata.json"));
            return jsonArray;
        } catch (IOException | ParseException ie) {
            System.out.println(ie);
        }
        return null;
    }

    //get the price of each product in float;
    public static float getPriceInFloat(JSONObject tmpObj) {
        String price = (String) tmpObj.get("price");
        //String subString = price.substring(1);
        //System.out.println(subString);
        String priceInString = price.replaceAll("[$,]", "");
        //  System.out.println(priceInString);
        float priceInFloat = Float.parseFloat(priceInString);
        System.out.println("O preço do produto é: " + priceInFloat);
        return priceInFloat;
    }

    // calculate the total weekly income
    public static void calcWeeklyIncome(Calendar myDate, float priceInFloat) {
        int i = getDayOfTheWeek(myDate);
        weeklyIncome[i - 1] = weeklyIncome[i - 1] + priceInFloat;

    }
// calculate the total turnover of each product

    public static void calcProductTurnover(float idProductInFloat, float priceInFloat) {
        int idProductInteger = (int) idProductInFloat;
        productTurnover[idProductInteger] = productTurnover[idProductInteger] + priceInFloat;
    }

    public static Calendar getDate(String orderDate) {
        try {
            Calendar myDate = new GregorianCalendar();
            Date thedate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(orderDate);
            myDate.setTime(thedate);
            System.out.println("year   -> " + myDate.get(Calendar.YEAR));
            System.out.println("month  -> " + myDate.get(Calendar.MONTH));
            System.out.println("dom    -> " + myDate.get(Calendar.DAY_OF_MONTH));
            System.out.println("dow    -> " + myDate.get(Calendar.DAY_OF_WEEK));
            return myDate;
        } catch (java.text.ParseException ex) {
            System.out.println(ex);
        }
        return null;
    }

    public static String getOrderDate(JSONObject tmpObj) {
        String orderDate = (String) (tmpObj.get("order_date"));
        return orderDate;
    }

    // get a return integer for each day of the week
    public static int getDayOfTheWeek(Calendar myDate) {
        switch (myDate.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 6;
            case 7:
                return 7;
            default:
                break;
        }
        return 0;

    }

    //get the id of each product
    public static float getProductIDInFloat(JSONObject tmpObj) {
        Long idProduct = (Long) (tmpObj.get("id_product"));
        float idProductInFloat = (float) idProduct;
        System.out.println("O id do produto é: " + idProductInFloat);
        return idProductInFloat;
    }

    public static void invalidProduct(float priceInFloat) {
        if (priceInFloat == 0) {
            System.out.println("O produto é inválido");
        }
    }

}
